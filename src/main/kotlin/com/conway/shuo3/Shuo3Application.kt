package com.conway.shuo3

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Shuo3Application

fun main(args: Array<String>) {
	runApplication<Shuo3Application>(*args)
}
