package com.conway.shuo3.dto.json

import com.conway.shuo3.dto.LikeType

/** Created by Conway */
class LikeRequest(val likeType: LikeType)