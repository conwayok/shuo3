package com.conway.shuo3.dto.json

/** Created by Conway */
data class CustomResponse<T>(val code: Int, val message: String, var payload: T? = null)