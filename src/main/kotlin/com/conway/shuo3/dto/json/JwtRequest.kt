package com.conway.shuo3.dto.json

/** Created by Conway */
data class JwtRequest(val username: String, val password: String)