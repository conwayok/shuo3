package com.conway.shuo3.dto.json

/** Created by Conway */
data class PostIdResponse(val postId: String)