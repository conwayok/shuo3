package com.conway.shuo3.dto.json

/** Created by Conway */
data class JwtResponse(val token: String)