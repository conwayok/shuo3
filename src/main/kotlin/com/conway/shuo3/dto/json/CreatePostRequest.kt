package com.conway.shuo3.dto.json

/** Created by Conway */
data class CreatePostRequest(val title: String, val textContent: String?, val images: List<String>?)