package com.conway.shuo3.dto

/** Created by Conway */
enum class LikeType {
  UPVOTE, DOWNVOTE
}